#include "Filter.h"
#include "Input/InputHandler.h"
#include "FocusManager.h"
#include "Papyrus.h"
#include "API/Compatibility.h"

bool Filter::DoesConditionApply(ConnectedCondition& condition, RE::TESObjectREFR* objectRef)
{
    auto playerChar = RE::PlayerCharacter::GetSingleton();
    if (!playerChar || !objectRef)
        return false;

	bool conditionResult = false;

	switch (condition.Condition)
	{
    case ECondition::NONE:
		conditionResult = true;
        break;
	case ECondition::IsSneaking:
		conditionResult = playerChar->IsSneaking();
        break;
	case ECondition::IsInThirdPerson:
		conditionResult = !Util::IsPlayerInFirstPerson();
        break;
	case ECondition::IsInFirstPerson:
		conditionResult = Util::IsPlayerInFirstPerson();
        break;
	case ECondition::IsInCombat:
        conditionResult = playerChar->IsInCombat();
        break;
	case ECondition::IsWeaponDrawn:
        conditionResult = playerChar->AsActorState()->IsWeaponDrawn();
        break;
	case ECondition::IsOnHorseback:
        conditionResult = Util::GetPlayerMountRef();
        break;
	case ECondition::IsCrimeToActivate:
		conditionResult = objectRef->IsCrimeToActivate();
        break;
	case ECondition::IsKeyDown:
        conditionResult = DoesConditionApply_IsKeyDown(condition);
        break;
	case ECondition::IsKeyToggled:
        conditionResult = DoesConditionApply_IsKeyToggled(condition);
        break;
	case ECondition::IsDisplayName:
        conditionResult = DoesConditionApply_IsDisplayName(condition, objectRef);
        break;
	case ECondition::IsSwimming:
        conditionResult = playerChar->AsActorState()->IsSwimming();
        break;
	case ECondition::IsSprinting:
        conditionResult = playerChar->AsActorState()->IsSprinting();
        break;
	case ECondition::IsFlying:
        conditionResult = playerChar->AsActorState()->IsFlying();
        break;
	case ECondition::IsSitting:
        conditionResult = playerChar->AsActorState()->GetSitSleepState() == RE::SIT_SLEEP_STATE::kIsSitting;
        break;
	case ECondition::IsBaseID:
        conditionResult = DoesConditionApply_IsBaseID(condition, objectRef);
        break;
	case ECondition::IsValueEqual:
		conditionResult = DoesConditionApply_IsValueEqual(condition, objectRef);
        break;
	case ECondition::IsValueMore:
		conditionResult = DoesConditionApply_IsValueMore(condition, objectRef);
        break;
	case ECondition::IsValueLess:
		conditionResult = DoesConditionApply_IsValueLess(condition, objectRef);
        break;
	case ECondition::IsWeightEqual:
		conditionResult = DoesConditionApply_IsWeightEqual(condition, objectRef);
        break;
	case ECondition::IsWeightMore:
		conditionResult = DoesConditionApply_IsWeightMore(condition, objectRef);
        break;
	case ECondition::IsWeightLess:
		conditionResult = DoesConditionApply_IsWeightLess(condition, objectRef);
        break;
	case ECondition::HasKeyword:
		conditionResult = DoesConditionApply_HasKeyword(condition, objectRef);
		break;
	case ECondition::IsScriptAttached:
		conditionResult = DoesConditionApply_IsScriptAttached(condition, objectRef);
		break;
	case ECondition::IsDetected:
		conditionResult = DoesConditionApply_IsDetected(condition);
		break;
	case ECondition::IsHorse:
		conditionResult = objectRef->IsHorse();
		break;
	case ECondition::IsCrosshairSelection:
		conditionResult = objectRef == FocusManager::FocusRef_Orig;
		break;
	case ECondition::IsActor:
		conditionResult = DoesConditionApply_IsActor(condition, objectRef);
		break;
	case ECondition::IsDead:
		conditionResult = DoesConditionApply_IsDead(condition, objectRef);
		break;
	case ECondition::IsInBleedOut:
		conditionResult = DoesConditionApply_IsInBleedOut(condition, objectRef);
		break;
	case ECondition::IsUsingBTPSSelection:
		conditionResult = FocusManager::IsEnabled;
		break;
	case ECondition::IsUsingNativeSelection:
		conditionResult = !FocusManager::IsEnabled;
		break;
	case ECondition::IsInBeastForm:
		conditionResult = DoesConditionApply_IsInBeastForm(condition);
		break;
	case ECondition::IsBook:
	{
		auto boundObj = objectRef->GetObjectReference();
		if (!boundObj)
			conditionResult = false;
		else
		{

			auto book = boundObj->As<RE::TESObjectBOOK>();
			conditionResult = book != nullptr;
		}
		break;
	}
	case ECondition::IsBookRead:
	{
		auto boundObj = objectRef->GetObjectReference();
		if (!boundObj)
			conditionResult = false;
		else
		{
			auto book = boundObj->As<RE::TESObjectBOOK>();
			conditionResult = (book && book->IsRead());
		}
		break;
	}
	case ECondition::IsSkillBook:
	{
		auto boundObj = objectRef->GetObjectReference();
		if (!boundObj)
			conditionResult = false;
		else
		{
			auto book = boundObj->As<RE::TESObjectBOOK>();
			conditionResult = (book && book->TeachesSkill());
		}
		break;
	}
	case ECondition::IsSpellBook:
	{
		auto boundObj = objectRef->GetObjectReference();
		if (!boundObj)
			conditionResult = false;
		else
		{
			auto book = boundObj->As<RE::TESObjectBOOK>();
			conditionResult = (book && book->TeachesSpell());
		}
		break;
	}
	case ECondition::IsEmptyContainer:
		conditionResult = DoesConditionApply_IsEmptyContainer(condition, objectRef);
		break;
	}

	if (condition.Inverse)
        conditionResult = !conditionResult;

	return conditionResult;
}

bool Filter::DoConditionsApply(std::vector<ConnectedCondition>& conditions, RE::TESObjectREFR* objectRef)
{
    bool conditionResult = true;
    EConditionConnector LastConnector = EConditionConnector::NONE;

	for (int i = 0; i < conditions.size(); i++)
    {
        ConnectedCondition& condition = conditions[i];

		switch (LastConnector)
		{
        case EConditionConnector::NONE:
            conditionResult = DoesConditionApply(condition, objectRef);
            break;

        case EConditionConnector::OR:
            conditionResult |= DoesConditionApply(condition, objectRef);
			break;

		case EConditionConnector::AND:
            conditionResult &= DoesConditionApply(condition, objectRef);
			break;
		}

		LastConnector = condition.Connector;
	}

    return conditionResult;
}

// taken from PowerOf3 script extender
bool Filter::DoesConditionApply_IsDetected(ConnectedCondition& condition)
{
	/*auto ui = RE::UI::GetSingleton();
	if (!ui)
		return false;

	auto hud = ui->GetMenu<RE::HUDMenu>(RE::HUDMenu::MENU_NAME);
	if (!hud)
		return false;

	auto stealthMeter = static_cast<RE::StealthMeter*>(hud->objects[RE::HUDObject::HudComponent::kStealthMeterInstance]);
	if (!stealthMeter)
		return false;

	logger::info("Player Detection Level: {}", stealthMeter->unk88);
	return stealthMeter->unk88 == 100;*/

	auto playerChar = RE::PlayerCharacter::GetSingleton();

	if (playerChar->GetActorRuntimeData().currentProcess)
	{
		const auto processLists = RE::ProcessLists::GetSingleton();
		if (processLists)
		{
			for (auto& targetHandle : processLists->highActorHandles)
			{
				auto target = targetHandle.get();
				if (target && target->GetActorRuntimeData().currentProcess && target->RequestDetectionLevel(playerChar) > 0)
					return true;
			}
		}
	}

	return false;
}

bool Filter::DoesConditionApply_IsKeyDown(ConnectedCondition& condition)
{
     int keyCode = -1;
	if (!condition.GetParam(0, keyCode))
	{
		logger::warn("BTPS: filterPreset failed on IsKeyDown, invalid parameter at idx 0");
        return false;
	}

    if (keyCode < 0)
        return false;

    auto inputHandler = InputHandler::GetSingleton();
    if (!inputHandler)
        return false;

    return inputHandler->IsKeyPressed(keyCode);
}

bool Filter::DoesConditionApply_IsKeyToggled(ConnectedCondition& condition)
{
    int keyCode = -1;
	if (!condition.GetParam(0, keyCode))
	{
		logger::warn("BTPS: filterPreset failed on IsKeyToggled, invalid parameter at idx 0");
        return false;
	}

    if (keyCode < 0)
        return false;

    auto inputHandler = InputHandler::GetSingleton();
    if (!inputHandler)
        return false;

    return inputHandler->IsKeyToggled(keyCode);
}

bool Filter::DoesConditionApply_IsDisplayName(ConnectedCondition& condition, RE::TESObjectREFR* objectRef)
{
    if (condition.Parameters.empty())
        return false;

	std::string displayName = objectRef->GetDisplayFullName();
    Util::ToLowerString(displayName);

	for (int i = 0; i < condition.Parameters.size(); i++)
	{
		std::string currDisplayName;
		if (!condition.GetParam(i, currDisplayName))
			continue;

        Util::ToLowerString(currDisplayName);

		if (currDisplayName == displayName)
            return true;
	}

    return false;
}

bool Filter::DoesConditionApply_IsBaseID(ConnectedCondition& condition, RE::TESObjectREFR* objectRef)
{
	auto boundObj = objectRef->GetObjectReference();
	if (!boundObj)
		return false;

	auto formID = boundObj->GetFormID();

	for (int i = 0; i < condition.Parameters.size(); i++)
	{
		int baseID;
		if (!condition.GetParam(i, baseID))
			continue;

		if (formID == (unsigned int)baseID)
			return true;
	}

	return false;
}

bool Filter::DoesConditionApply_IsInBeastForm(ConnectedCondition & condition)
{
	auto menuCtrls = RE::MenuControls::GetSingleton();
	return menuCtrls && menuCtrls->InBeastForm();
}

bool Filter::DoesConditionApply_HasKeyword(ConnectedCondition& condition, RE::TESObjectREFR* objectRef)
{
	auto boundObj = objectRef->GetObjectReference();
	if (!boundObj)
		return false;

	auto keywordForm = skyrim_cast<RE::BGSKeywordForm*>(boundObj);
	if (!keywordForm)
		return false;

	for (int i = 0; i < condition.Parameters.size(); i++)
	{
		std::string keyword;
		if (!condition.GetParam(i, keyword))
			continue;

		if (keywordForm->HasKeywordString(keyword))
			return true;
	}

	return false;
}

bool Filter::DoesConditionApply_IsScriptAttached(ConnectedCondition& condition, RE::TESObjectREFR* objectRef)
{
	auto boundObj = objectRef->GetObjectReference();
	if (!boundObj)
		return false;

	auto objForm = boundObj->As<RE::TESForm>();
	if (!objForm)
		return false;

	std::string scriptName;
	if (!condition.GetParam(0, scriptName))
	{
		logger::warn("BTPS: filter preset '{}' failed on function HasScript - missing parameter 0 (string)");
		return false;
	}

	return Papyrus::IsScriptAttached(objForm, scriptName);
}

bool Filter::DoesConditionApply_IsActor(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	return objectRef && objectRef->As<RE::Actor>();
}

bool Filter::DoesConditionApply_IsDead(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	if (!objectRef)
		return false;

	auto actor = objectRef->As<RE::Actor>();
	if (!actor)
		return false;

	auto lifeState = actor->AsActorState()->GetLifeState();
	return lifeState == RE::ACTOR_LIFE_STATE::kDead || lifeState == RE::ACTOR_LIFE_STATE::kDying;
}

bool Filter::DoesConditionApply_IsInBleedOut(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	if (!objectRef)
		return false;

	auto actor = objectRef->As<RE::Actor>();
	if (!actor)
		return false;

	auto lifeState = actor->AsActorState()->GetLifeState();
	return lifeState == RE::ACTOR_LIFE_STATE::kBleedout || lifeState == RE::ACTOR_LIFE_STATE::kEssentialDown;
}

bool Filter::DoesConditionApply_IsUnconscious(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	if (!objectRef)
		return false;

	auto actor = objectRef->As<RE::Actor>();
	if (!actor)
		return false;

	return actor->AsActorState()->GetLifeState() == RE::ACTOR_LIFE_STATE::kUnconcious;
}

bool Filter::DoesConditionApply_IsRestrained(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	if (!objectRef)
		return false;

	auto actor = objectRef->As<RE::Actor>();
	if (!actor)
		return false;

	return actor->AsActorState()->GetLifeState() == RE::ACTOR_LIFE_STATE::kRestrained;
}

bool Filter::DoesConditionApply_IsReanimated(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	if (!objectRef)
		return false;

	auto actor = objectRef->As<RE::Actor>();
	if (!actor)
		return false;

	return actor->AsActorState()->GetLifeState() == RE::ACTOR_LIFE_STATE::kReanimate;
}

bool Filter::DoesConditionApply_IsValueEqual(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	auto boundObj = objectRef->GetObjectReference();
	if (!boundObj)
		return false;

	int valueParam = 0;
	if (!condition.GetParam(0, valueParam))
	{
		logger::warn("BTPS: FilterPreset '{}' failed on 'IsValueEqual', invalid param at idx 0 ", condition.GetParentName());
		return false;
	}

	return objectRef->GetGoldValue() == valueParam;
}

bool Filter::DoesConditionApply_IsValueMore(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	auto boundObj = objectRef->GetObjectReference();
	if (!boundObj)
		return false;

	int valueParam = 0;
	if (!condition.GetParam(0, valueParam))
	{
		logger::warn("BTPS: FilterPreset '{}' failed on 'IsValueMore', invalid param at idx 0 ", condition.GetParentName());
		return false;
	}

	return boundObj->GetGoldValue() > valueParam;
}

bool Filter::DoesConditionApply_IsValueLess(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	auto boundObj = objectRef->GetObjectReference();
	if (!boundObj)
		return false;

	int valueParam = 0;
	if (!condition.GetParam(0, valueParam))
	{
		logger::warn("BTPS: FilterPreset '{}' failed on 'IsValueLess', invalid param at idx 0", condition.GetParentName());
		return false;
	}

	return boundObj->GetGoldValue() < valueParam;
}

bool Filter::DoesConditionApply_IsWeightEqual(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	auto boundObj = objectRef->GetObjectReference();
	if (!boundObj)
		return false;

	float weightParam = 0;
	if (!condition.GetParam(0, weightParam))
	{
		logger::warn("BTPS: FilterPreset '{}' failed on 'IsWeightEqual', invalid param at idx 0", condition.GetParentName());
		return false;
	}

	return Util::IsRoughlyEqual(boundObj->GetWeight(), weightParam, 0.001f);
}

bool Filter::DoesConditionApply_IsWeightMore(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	auto boundObj = objectRef->GetObjectReference();
	if (!boundObj)
		return false;

	float weightParam = 0;
	if (!condition.GetParam(0, weightParam))
	{
		logger::warn("BTPS: FilterPreset '{}' failed on 'IsWeightMore', invalid param at idx 0", condition.GetParentName());
		return false;
	}

	return boundObj->GetWeight() > weightParam;
}

bool Filter::DoesConditionApply_IsWeightLess(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	auto boundObj = objectRef->GetObjectReference();
	if (!boundObj)
		return false;

	float weightParam = 0;
	if (!condition.GetParam(0, weightParam))
	{
		logger::warn("BTPS: FilterPreset '{}' failed on 'IsWeightLess', invalid param at idx 0", condition.GetParentName());
		return false;
	}

	return boundObj->GetWeight() < weightParam;
}

bool Filter::DoesConditionApply_IsEmptyContainer(ConnectedCondition & condition, RE::TESObjectREFR * objectRef)
{
	// using the method from QuickLoot compatibility out of laziness
	return Compatibility::QuickLootRE::IsEmpty(objectRef);
}

bool Filter::ShouldFilterFormType(RE::FormType formType, bool shouldFilterPrevious)
{
	bool shouldFilter = shouldFilterPrevious;

    for (int i = 0; i < FormTypes.size(); i++)
    {
        auto currFormType = FormTypes[i];

		if (currFormType.FormType == formType || 
			currFormType.FormType == RE::FormType::Max)
		{
			if (shouldFilter)
			{
				if (!currFormType.Remove)
					shouldFilter = true;
			}
			else
				shouldFilter = currFormType.Remove;
		}
    }

	return shouldFilter;
}

ECondition Filter::MakeCondition(std::string conditionName, bool &inverseOut)
{
    Util::StripWhiteSpaces(conditionName);
    
	if (!conditionName.empty())
    {
        if (conditionName[0] == '!')
        {
            inverseOut = true;
            conditionName.erase(0, 1);
        }
        else
            inverseOut = false;

		auto condition = magic_enum::enum_cast<ECondition>(conditionName);
		if (!condition.has_value())
		{
			logger::warn("BTPS: invalid function name '{}'", conditionName);
			return ECondition::NONE;
		}

		return condition.value();
    }

	logger::warn("BTPS: empty function name", conditionName);
	return ECondition::NONE;
}

EConditionConnector Filter::MakeConditionConnector(std::string conditionConnectorName)
{
    if (conditionConnectorName == "and") return EConditionConnector::AND;
    if (conditionConnectorName == "or") return EConditionConnector::OR;

	return EConditionConnector::NONE;
}

ConnectedCondition::ConnectedCondition(std::shared_ptr<Filter> parent,ECondition condition, EConditionConnector connector, bool inverse, std::vector<std::string> parameters)
{
	Parent = parent;
    Condition = condition;
    Connector = connector;
    Inverse = inverse;

	for (std::string& currString : parameters)
	{
		FilterParam newParam(currString);
		Parameters.push_back(newParam);
	}
}

bool ConnectedCondition::GetParam(int idx, int& valueOut)
{
	if (idx >= Parameters.size())
		return false;

	auto param = Parameters[idx];
	if (!param.IntValue)
		return false;

	valueOut = param.IntValue.value();
	return true;
}

bool ConnectedCondition::GetParam(int idx, float& valueOut)
{
	if (idx >= Parameters.size())
		return false;

	auto param = Parameters[idx];
	if (!param.FloatValue)
		return false;

	valueOut = param.FloatValue.value();
	return true;
}

bool ConnectedCondition::GetParam(int idx, bool& valueOut)
{
	if (idx >= Parameters.size())
		return false;

	auto param = Parameters[idx];
	if (!param.BoolValue)
		return false;

	valueOut = param.BoolValue.value();
	return true;
}

bool ConnectedCondition::GetParam(int idx, std::string & valueOut)
{
	if (idx >= Parameters.size())
		return false;

	auto param = Parameters[idx];
	if (!param.StringValue)
		return false;

	valueOut = param.StringValue.value();
	return true;
}

std::string ConnectedCondition::GetParentName()
{
	if (!Parent)
		return "<NONE>";

	return Parent->FilterName;
}
