#include "BTPS_API.h"

#include "FocusManager.h"
#include "Settings.h"
#include "UI/SelectionWidget.h"

BTPS_API* BTPS_API::GetSingleton()
{
    static BTPS_API singleton;
    return std::addressof(singleton);
}

bool BTPS_API::SelectionEnabled()
{
    return FocusManager::IsEnabled;
}

bool BTPS_API::Widget3DEnabled()
{
    return Settings::Widget3DEnabled;
}


void BTPS_API::HideSelectionWidget(std::string source)
{
    SelectionWidgetMenu::Hide(source);
}

void BTPS_API::ShowSelectionWidget(std::string source)
{
    SelectionWidgetMenu::Show(source);
}
