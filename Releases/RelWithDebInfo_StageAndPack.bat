set version="0.6.0"

xcopy /s/e/y/v "..\Interface" "Stage\Data\Interface\"
xcopy /s/e/y/v "..\MCM" "Stage\Data\MCM\"
xcopy /s/e/y/v "..\scripts\scripts" "Stage\Data\scripts\"
xcopy /s/e/y/v "..\scripts\source" "Stage\Data\scripts\source\"

xcopy /F/y "..\BetterThirdPersonSelection.esp" "Stage\Data\"

xcopy /F/y "..\build\RelWithDebInfo\BetterThirdPersonSelection.dll" "Stage\Data\SKSE\Plugins\"
xcopy /F/y "..\build\RelWithDebInfo\BetterThirdPersonSelection.pdb" "PDBs\BTPS_%version%.pdb*"

cd "Stage"
"D:\Program Files\7-Zip\7z.exe" a "..\Rel\BTPS_%version%.7z" "*"

pause
