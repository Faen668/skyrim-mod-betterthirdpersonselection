option(BUILD_DEBUG "Enable debugging options" ON)

set(SKSE_SUPPORT_XBYAK ON)
set(ENABLE_SKYRIM_VR OFF)
set(BUILD_TESTS OFF)

set(CommonLibPath ".git/modules/external/CommonLibNG")
set(CommonLibName "CommonLibNG")

get_filename_component(
	Skyrim64Path
	"[HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\Bethesda Softworks\\Skyrim Special Edition;installed path]"
	ABSOLUTE CACHE
)

if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
	set(CMAKE_INSTALL_PREFIX "${Skyrim64Path}/Data" CACHE PATH
		"Install path prefix (e.g. Skyrim Data directory or Mod Organizer virtual directory)."
		FORCE)
endif()

add_library("${PROJECT_NAME}" SHARED)

target_compile_features(
	"${PROJECT_NAME}"
	PRIVATE
		cxx_std_23
)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

include(AddCXXFiles)
add_cxx_files("${PROJECT_NAME}")

configure_file(
	${CMAKE_CURRENT_SOURCE_DIR}/cmake/Plugin.h.in
	${CMAKE_CURRENT_BINARY_DIR}/cmake/Plugin.h
	@ONLY
)

configure_file(
	${CMAKE_CURRENT_SOURCE_DIR}/cmake/version.rc.in
	${CMAKE_CURRENT_BINARY_DIR}/cmake/version.rc
	@ONLY
)

target_sources(
	"${PROJECT_NAME}"
	PRIVATE
		${CMAKE_CURRENT_BINARY_DIR}/cmake/Plugin.h
		${CMAKE_CURRENT_BINARY_DIR}/cmake/version.rc
		.clang-format
		.editorconfig
)

target_precompile_headers(
	"${PROJECT_NAME}"
	PRIVATE
		src/PCH.h
)

if (CMAKE_GENERATOR MATCHES "Visual Studio")
	option(CMAKE_VS_INCLUDE_INSTALL_TO_DEFAULT_BUILD "Include INSTALL target to default build." OFF)
	target_compile_options(
		"${PROJECT_NAME}"
		PRIVATE
			"/sdl"	# Enable Additional Security Checks
			"/utf-8"	# Set Source and Executable character sets to UTF-8
			"/Zi"	# Debug Information Format

			"/permissive-"	# Standards conformance
			"/Zc:preprocessor"	# Enable preprocessor conformance mode

			"/wd4200" # nonstandard extension used : zero-sized array in struct/union
			"/wd4201" # nonstandard extension used : nameless struct/union
			"/wd4100" # unreferenced formal parameter
			
			"/W4" # Warning level

			"$<$<CONFIG:DEBUG>:>"
			"$<$<CONFIG:RELEASE>:/Zc:inline;/JMC->"
	)

	target_link_options(
		"${PROJECT_NAME}"
		PRIVATE
			"$<$<CONFIG:DEBUG>:/INCREMENTAL;/OPT:NOREF;/OPT:NOICF>"
			"$<$<CONFIG:RELEASE>:/INCREMENTAL:NO;/OPT:REF;/OPT:ICF;/DEBUG>"
			"$<$<CONFIG:RelWithDebInfo>:/DEBUG>"
	)
endif()

add_subdirectory(${CommonLibPath} ${CommonLibName} EXCLUDE_FROM_ALL)

find_package(spdlog CONFIG REQUIRED)

add_custom_command(
	TARGET ${PROJECT_NAME}
	POST_BUILD
	COMMAND xcopy "\"$(TargetDir)*$(TargetExt)\" \"$(Skyrim64Path)\\Data\\SKSE\\Plugins\\\" /F /R /Y /I"
)

find_package(jsoncpp CONFIG REQUIRED)

#set(JSON_INC_PATH vcpkg_installed\x64-windows-static-md\lib)
#link_directories(
	#${CMAKE_CURRENT_BINARY_DIR}/${LIB_INCL_PATH}
#)

set(TOML_INCL_PATH vcpkg_installed/x64-windows-static-md/include/toml++)
set(GLM_INCL_PATH vcpkg_installed/x64-windows-static-md/include/glm)
set(MAGIC_ENUM_INCL_PATH vcpkg_installed/x64-windows-static-md/include)
set(JSON_INCL_PATH vcpkg_installed/x64-windows-static-md/include/json)

target_include_directories(
	"${PROJECT_NAME}"
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/include
		${CMAKE_CURRENT_BINARY_DIR}/${TOML_INCL_PATH}
		${CMAKE_CURRENT_BINARY_DIR}/${GLM_INCL_PATH}
		${CMAKE_CURRENT_BINARY_DIR}/${MAGIC_ENUM_INCL_PATH}
		${CMAKE_CURRENT_BINARY_DIR}/${JSON_INCL_PATH}
	PRIVATE
		${CMAKE_CURRENT_BINARY_DIR}/cmake
		${CMAKE_CURRENT_SOURCE_DIR}/src
)

set(JSON_LIB_PATH_DEB vcpkg_installed/x64-windows-static-md/debug/lib)
set(JSON_LIB_PATH vcpkg_installed/x64-windows-static-md/lib)

target_link_libraries(
	"${PROJECT_NAME}"
	PUBLIC
	optimized ${CMAKE_CURRENT_BINARY_DIR}/${JSON_LIB_PATH}/jsoncpp.lib
	debug ${CMAKE_CURRENT_BINARY_DIR}/${JSON_LIB_PATH_DEB}/jsoncpp.lib
)

