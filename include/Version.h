#pragma once

namespace Version
{
	inline constexpr std::size_t MAJOR = 0;
	inline constexpr std::size_t MINOR = 6;
	inline constexpr std::size_t PATCH = 0;
	inline constexpr auto NAME = "0.6.0"sv;
	inline constexpr auto PROJECT = "BetterThirdPersonSelection"sv;
}
